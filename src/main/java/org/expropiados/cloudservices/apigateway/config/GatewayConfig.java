package org.expropiados.cloudservices.apigateway.config;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GatewayConfig {
    private final AuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("backoffice-service", r -> r
                        .path("/backoffice-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://backoffice-service")
                )
                .route("attention-service", r -> r
                        .path("/attention-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://attention-service")
                )
                .route("sentiment-service", r -> r
                        .path("/sentiment-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://sentiment-service")
                )
                .route("security-service", r -> r
                        .path("/security-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://security-service")
                )
                .build();
    }
}
